package week2gradedassignment.com;

import java.util.ArrayList;
import java.util.HashMap;

public class DataStructureB {
	public void cityNameCourt(ArrayList<Employee> employees) {
		HashMap<String, Integer> hashMap=new HashMap<>();
		for (Employee employee : employees) {
			if(hashMap.containsKey(employee.getCity())) {
				Integer cityCount=hashMap.get(employee.getCity());
				hashMap.put(employee.getCity(), ++cityCount);
			}else {
				hashMap.put(employee.getCity(), 1);
			}
		}
		System.out.println(hashMap);
	}
	public void monthlySalary(ArrayList<Employee> employees) {
		HashMap<Integer, Double> hashMap=new HashMap<>();
		for (Employee employee : employees) {
			hashMap.put(employee.getId(), (double)(employee.getSalary()/12));
		}
		System.out.println(hashMap);
	}
	
	public static void main(String[] args) {
		ArrayList<Employee> employees=new ArrayList<>();
		DataStructureA dataStructureA=new DataStructureA();
		DataStructureB dataStructureB=new DataStructureB();
		try {
			
			employees.add(new Employee(1,"Ajay",20,1100000,"IT","Delhi")) ;
			employees.add(new Employee(2,"Satya",22,500000,"HR","Bombay"));
			employees.add(new Employee(3,"Zoe",20,750000,"Admin","Delhi"));
			employees.add(new Employee(4,"Smitha",21,1000000,"IT","Chennai"));
			employees.add(new Employee(5,"Smitha",24,1200000,"HR","Bengaluru"));
			
			for (Employee employee : employees) {
				if(employee.getId()<0 || employee.getName()==null || employee.getAge()<0 || employee.getSalary()<0 || employee.getDepartment()==null || employee.getCity()==null)
					throw new IllegalArgumentException();
			}
			
			//firts question
			System.out.println(employees);
			
			//second question
			dataStructureA.sortingNames(employees);
			
			
			//third question
			dataStructureB.cityNameCourt(employees);
			
			//fourth queston
			dataStructureB.monthlySalary(employees);
			
		}catch(IllegalArgumentException e) {
			System.out.println(e);
		}
		
	}
}