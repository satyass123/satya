package week2gradedassignment.com;

import java.util.ArrayList;
import java.util.Collections;

public class DataStructureA {
	public void sortingNames(ArrayList<Employee> employees) {
		ArrayList<String> employeeNames=new ArrayList<>();
		Collections.sort(employees,(employee1,employee2)->employee1.name.compareTo(employee2.name));
		for (Employee employee : employees) {
			employeeNames.add(employee.getName());
		}
		System.out.println(employeeNames);
	}
}